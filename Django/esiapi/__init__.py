#!/usr/bin/python
from esiapi.character import *
import datetime
import esiapi.settings
import json
import requests
import settings
try:
	from urllib import urlencode, unquote
	from urlparse import urlparse, parse_qsl, ParseResult
except ImportError:
	# Python 3 fallback
	from urllib.parse import (
		urlencode, unquote, urlparse, parse_qsl, ParseResult
	)
#from esioauth.models import AuthID

API_SETTINGS = settings.ESI_APP_SETTINGS

callback_uri = API_SETTINGS['callback_uri']
auth = (API_SETTINGS['client_id'], API_SETTINGS['client_secret'])
api_uri = API_SETTINGS['api_uri']
if (not auth) or (not callback_uri):
	raise IndexError("ESI APP settings missing, please fill settings.py\n\
with relevant information, for more details: https://developers.eveonline.com/")

class Api(object):
	"""Parent of all API endpoints"""
	def __init__(self, token):
		self.token = token

	def _url(self, path, parameters={}):
		"""Constructs full url using base url and path.
		Accepts url parameters as a dictionary."""
		# define url object from base url
		url = urlparse(api_uri)
		url_args = url.query
		url_args_dict = dict(parse_qsl(url_args))
		url_args_dict.update(parameters)
		if not 'datasource' in url_args_dict:
			url_args_dict['datasource'] = 'tranquility'
		enc_url_args = urlencode(url_args_dict, doseq=True)
		full_path = url.path + path
		# preventing double slash '//' as this can break things
		clean_path = filter(None, full_path.split('/'))
		new_path = '/'.join(clean_path)
		new_url = ParseResult(url.scheme, url.netloc, new_path, url.params, enc_url_args, url.fragment).geturl()
		return new_url

	def get(self, path, parameters={}, request_data='', headers=''):
		if self.token:
			parameters['token'] = self.token
		response = requests.get(self._url(path, parameters), data=request_data, headers=headers)
		return response

	def post(self, path, parameters={}, request_data='', headers={}):
		if self.token:
			parameters['token'] = self.token
		response = requests.post(self._url(path, parameters), data=request_data, headers=headers)
		return response
