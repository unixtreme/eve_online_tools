#!/usr/bin/python
"""Character API"""
from __init__ import *

class Character(Api):
	def __init__(self, token):
		self.base_path = '/characters/'
		self.token = token

	def get(self, *args, **kwargs):
		response = super(Character, self).get(*args, **kwargs)
		return response

	def post(self, *args, **kwargs):
		response = super(Character, self).post(*args, **kwargs)
		return response

	def _url(self, *args, **kwargs):
		new_url = super(Character, self)._url(*args, **kwargs)		
		return new_url

	def get_standings(self, character_id):
		path = self.base_path + character_id + '/standings/'
		response = self.get(path)
		return response