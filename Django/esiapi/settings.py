# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# esioauth app configuration file. While I would've liked to keep this on the
# default setting structure I decided to do it here for portability reasons
# 

ESI_APP_SETTINGS = {'api_uri': 'https://esi.tech.ccp.is/latest/',
                'client_id': '',
                'client_secret': '',
                'callback_uri': ''
                }
